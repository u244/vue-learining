import { createApp } from 'vue'
import App from './App.vue'
import './scss/main.scss'
import 'remixicon/fonts/remixicon.css'


createApp(App).mount('#app')
